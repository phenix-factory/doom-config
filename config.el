;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
;; (setq user-full-name "John Doe"
;;       user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "Source Code Pro" :size 16 :weight 'semi-light)
     doom-variable-pitch-font (font-spec :family "Source Code Pro" :size 16))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-dracula)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(setq mu4e-context-policy 'ask
      mu4e-compose-context-policy 'ask
      mu4e-maildir "~/.mail")

(set-email-account! "henix.be"
                    '(
                      (user-mail-address      . "p@henix.be")
                      (user-full-name         . "Debondt Didier" )
                      (mu4e-get-mail-command  . "mbsync -a")
                      (mu4e-view-show-images . "t")
                      (mu4e-sent-folder . "/p@henix.be/Sent")
                      (mu4e-drafts-folder . "/p@henix.be/Drafts")
                      (mu4e-trash-folder . "/p@henix.be/Trash")
                      (mu4e-refile-folder . "/p@henix.be/Archives/2024")
                      (smtpmail-smtp-server . "mail.infomaniak.com")
                      (smtpmail-smtp-service . 587)
                      (smtpmail-stream-type . starttls)
                      ))
(set-email-account! "odoo.com"
                    '(
                      (user-mail-address      . "did@odoo.com")
                      (user-full-name         . "Debondt Didier" )
                      (mu4e-get-mail-command  . "mbsync -a")
                      (mu4e-view-show-images . "t")
                      (mu4e-sent-folder . "/did@odoo.com/Sent")
                      (mu4e-drafts-folder . "/did@odoo.com/Drafts")
                      (mu4e-trash-folder . "/did@odoo.com/Trash")
                      (mu4e-refile-folder . "/did@odoo.com/Archives/2024")
                      (smtpmail-smtp-server . "smtp.gmail.com")
                      (smtpmail-smtp-service . 587)
                      (smtpmail-stream-type . starttls)
                      ))
(set-email-account! "borderdens.com"
                    '(
                      (user-mail-address      . "didier@borderdens.com")
                      (user-full-name         . "Debondt Didier" )
                      (mu4e-get-mail-command  . "mbsync -a")
                      (mu4e-view-show-images . "t")
                      (mu4e-sent-folder . "/didier@borderdens.com/Sent")
                      (mu4e-drafts-folder . "/didier@borderdens.com/Drafts")
                      (mu4e-trash-folder . "/didier@borderdens.com/Trash")
                      (mu4e-refile-folder . "/didier@borderdens.com/Archives/2024")
                      (smtpmail-smtp-server . "smtp.mail.ovh.net")
                      (smtpmail-smtp-service . 465)
                      (smtpmail-stream-type . ssl)
                      ))


;; (setq mu4e-context-policy 'ask
;;       mu4e-compose-context-policy 'ask

;;       mu4e-maildir "~/.mail"

;;       ;; This fix the duplicate UID warning using mbsync
;;       mu4e-change-filenames-when-moving 't

;;       mu4e-enable-notifications 't

;;       ;; transformer le html en format text "presque" lisible
;;       ;; A mort le HTML dans les mails...
;;       mu4e-html2text-command '"/usr/bin/html2text -utf8 -width 72"

;;       ;; Afficher les adresse en plus du nom
;;       mu4e-view-show-addresses t

;;       send-mail-function (quote smtpmail-send-it)

;;       mu4e-headers-fields '((:human-date . 12) (:flags . 6) (:mailing-list . 10) (:from-or-to . 22) (:subject))
;;       mu4e-update-interval '300 ; toutes les 5 minutes
;;       mu4e-contexts
;;       `(
;;         ,(make-mu4e-context
;;           :name "p@henix.be"
;;           :enter-func (lambda () (mu4e-message "Enter p@henix.be context"))
;;           :leave-func (lambda () (mu4e-message "Leave p@henix.be context"))
;;           ;; Match based on the contact-fields of the message (that we are replying to)
;;           :match-func (lambda (msg) (when msg (mu4e-message-contact-field-matches msg :to "[a-z]+@henix.be")))
;;           :vars '(
;;                   (user-mail-address      . "p@henix.be")
;;                   (user-full-name         . "Debondt Didier" )
;;                   (mu4e-get-mail-command  . "mbsync -a")
;;                   (mu4e-view-show-images . "t")
;;                   (mu4e-sent-folder . "/p@henix.be/Sent")
;;                   (mu4e-drafts-folder . "/p@henix.be/Drafts")
;;                   (mu4e-trash-folder . "/p@henix.be/Trash")
;;                   (mu4e-refile-folder . "/p@henix.be/Archives/2024")
;;                   (smtpmail-smtp-server . "mail.infomaniak.com")
;;                   (smtpmail-smtp-service . 587)
;;                   (smtpmail-stream-type . starttls)
;;                   )
;;           )
;;         ,(make-mu4e-context
;;           :name "did@odoo.com"
;;           :enter-func (lambda () (mu4e-message "Enter did@odoo.com context"))
;;           :leave-func (lambda () (mu4e-message "Leave did@odoo.com context"))
;;           ;; Match based on the contact-fields of the message (that we are replying to)
;;           :match-func (lambda (msg)
;;                         (when msg
;;                           (mu4e-message-contact-field-matches msg
;;                                                               :to "[a-z]+@odoo.com")))
;;           :vars '(
;;                   (user-mail-address      . "did@odoo.com")
;;                   (user-full-name         . "Debondt Didier" )
;;                   (mu4e-get-mail-command  . "mbsync -a")
;;                   (mu4e-view-show-images . "t")
;;                   (mu4e-sent-folder . "/did@odoo.com/Sent")
;;                   (mu4e-drafts-folder . "/did@odoo.com/Drafts")
;;                   (mu4e-trash-folder . "/did@odoo.com/Trash")
;;                   (mu4e-refile-folder . "/did@odoo.com/Archives/2024")
;;                   (smtpmail-smtp-server . "smtp.gmail.com")
;;                   (smtpmail-smtp-service . 587)
;;                   (smtpmail-stream-type . starttls)
;;                   )
;;           )
;;         ,(make-mu4e-context
;;           :name "didier@borderdens.com"
;;           :enter-func (lambda () (mu4e-message "Enter didier@borderdens.com context"))
;;           :leave-func (lambda () (mu4e-message "Leave didier@borderdens.com context"))
;;           ;; Match based on the contact-fields of the message (that we are replying to)
;;           :match-func (lambda (msg)
;;                         (when msg
;;                           (mu4e-message-contact-field-matches msg
;;                                                               :to "[a-z]+@borderdens.com")))
;;           :vars '(
;;                   (user-mail-address      . "didier@borderdens.com")
;;                   (user-full-name         . "Debondt Didier" )
;;                   (mu4e-get-mail-command  . "mbsync -a")
;;                   (mu4e-view-show-images . "t")
;;                   (mu4e-sent-folder . "/didier@borderdens.com/Sent")
;;                   (mu4e-drafts-folder . "/didier@borderdens.com/Drafts")
;;                   (mu4e-trash-folder . "/didier@borderdens.com/Trash")
;;                   (mu4e-refile-folder . "/didier@borderdens.com/Archives/2024")
;;                   (smtpmail-smtp-server . "smtp.mail.ovh.net")
;;                   (smtpmail-smtp-service . 465)
;;                   (smtpmail-stream-type . ssl)
;;                   )
;;           )
;;         )
;;       )
